<?php

/**
 * @file
 * Primary module hooks for Paragraphs unpublished by default module.
 */

use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_form_FORM_ID_alter() for paragraphs_type_edit_form.
 *
 * Add the "Published by default" checkbox to the Edit form of the paragraphs
 * type and store the value of the checkbox in the ThirdPartySetting on submit.
 */
function paragraphs_unpublished_by_default_form_paragraphs_type_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\paragraphs\ParagraphsTypeInterface $paragraphs_type */
  $paragraphs_type = $form_state->getFormObject()->getEntity();

  $published_by_default = $paragraphs_type
    ->getThirdPartySetting('paragraphs_unpublished_by_default', 'published_by_default');

  $form['published_by_default'] = [
    '#type' => 'checkbox',
    '#title' => t('Published by default'),
    '#default_value' => $published_by_default,
  ];

  $form['#entity_builders'][] = 'paragraphs_unpublished_by_default_paragraphs_type_entity_builder';
}

/**
 * Paragraphs type entity builder callback.
 *
 * Store the value of the "Published by default" checkbox in the
 * ThirdPartySetting of paragraphs type when the "Edit" form of a paragraphs
 * type is submitted.
 */
function paragraphs_unpublished_by_default_paragraphs_type_entity_builder($entity_type, ParagraphsTypeInterface $paragraphs_type, &$form, FormStateInterface $form_state) {
  $paragraphs_type->setThirdPartySetting('paragraphs_unpublished_by_default', 'published_by_default', $form_state->getValue('published_by_default'));
}

/**
 * Implements hook_ENTITY_TYPE_create() for paragraphs_type.
 *
 * Set the value of the "Published by default" setting in 1 and store it the
 * ThirdPartySetting of the paragraphs type. We need this because when a user
 * created a new paragraphs type a user redirected to the Manage field page.
 * That is the Edit page dosen't opened and the  value "Published by default"
 * setting does'n strored in ThirdPartySetting. But we need the data from
 * ThirdPartySetting to set the value of the "Published" field of the newly
 * created paragraph.
 */
function paragraphs_unpublished_by_default_paragraphs_type_create(ParagraphsTypeInterface $paragraphs_type) {
  $paragraphs_type->setThirdPartySetting('paragraphs_unpublished_by_default', 'published_by_default', 1);
}

/**
 * Implements hook_ENTITY_TYPE_create() for paragraph.
 *
 * Unpublish paragraphs if the "Unpublished by default" option was set for its
 * paragraphs type. A user can override this option on the Edit form by checking
 * the "Published" checkbox (if the "Published" field is enabled in the Form
 * display).
 */
function paragraphs_unpublished_by_default_paragraph_create(ParagraphInterface $paragraph) {
  $paragraphs_type = $paragraph->getParagraphType();
  $published_by_default = $paragraphs_type
    ->getThirdPartySetting('paragraphs_unpublished_by_default', 'published_by_default');

  if (!$published_by_default) {
    $paragraph->setUnpublished();
  }
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 *
 * Set the value of the "Publish" field for the "paragraphs" widget.
 * If the "Published" field is present on the Form display of the paragraphs
 * type, then alter the value of this field based on the setting of the
 * paragraphs type.
 */
function paragraphs_unpublished_by_default_field_widget_single_element_paragraphs_form_alter(array &$element, FormStateInterface $form_state, array $context) {
  paragraphs_unpublished_by_default_alter_published_field($element, $context);
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 *
 * Set the value of the "Publish" field for the "entity_reference_paragraphs"
 * widget.  If the "Published" field is present on the Form display of the
 * paragraphs type, then alter the value of this field based on the setting of
 * the paragraphs type.
 */
function paragraphs_unpublished_by_default_field_widget_single_element_entity_reference_paragraphs_form_alter(array &$element, FormStateInterface $form_state, array $context) {
  paragraphs_unpublished_by_default_alter_published_field($element, $context);
}

/**
 * Alter the value of the "Published" field of the newly created paragraph.
 *
 * Alter the value of the "Published" field based on the setting of a paragraphs
 * type. The option is set only for new paragraphs and only if the "Published"
 * field is present on the form display of the paragraphs type.
 */
function paragraphs_unpublished_by_default_alter_published_field(array &$element, array $context) {
  // If the "Published" field is disabled, then do nothing.
  if (!isset($element['subform']['status']['widget']['value']['#default_value'])) {
    return;
  }

  $paragraph_is_new = $context['items']
    ->get($context['delta'])
    ->isEmpty();

  // If the paragraph is not new, then do nothing.
  if (!$paragraph_is_new) {
    return;
  }

  /** @var \Drupal\paragraphs\ParagraphsTypeInterface $paragraphs_type */
  $paragraphs_type = \Drupal::entityTypeManager()
    ->getStorage('paragraphs_type')
    ->load($element['#paragraph_type']);

  $published_by_default = $paragraphs_type
    ->getThirdPartySetting('paragraphs_unpublished_by_default', 'published_by_default');

  $element['subform']['status']['widget']['value']['#default_value'] = $published_by_default;
}

/**
 * Implements hook_help().
 */
function paragraphs_unpublished_by_default_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the paragraphs module.
    case 'help.page.paragraphs_unpublished_by_default':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The module enables to make newly created paragraphs unpublished by default.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<ol>';
      $output .= '<li>' . t('Visit admin/structure/paragraphs_type') . '</li>';
      $output .= '<li>' . t('Find the paragraphs type you need.') . '</li>';
      $output .= '<li>' . t('Select the "Edit" operation for this paragraphs type.') . '</li>';
      $output .= '<li>' . t('Scroll down the page and uncheck the "Published by default" checkbox.') . '</li>';
      $output .= '<li>' . t('Click on the "Save" button.') . '</li>';
      $output .= '<ol>';
      return $output;
  }
}
