# Paragraphs unpublished by default


## Introduction

The "Paragraphs unpublished by default" module enables to make newly created
paragraphs unpublished by default. The module adds the "Published by default"
checkbox to the "Edit" form of the paragraphs type.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/paragraphs_unpublished_by_default).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/paragraphs_unpublished_by_default).

## Requirements

This module requires the following module:

* Paragraphs - https://www.drupal.org/project/paragraphs


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings.
There is no configuration.


## How to Use

1. Visit admin/structure/paragraphs_type
2. Find the paragraphs type you need.
3. Select the "Edit" operation for this paragraphs type.
4. Scroll down the page and uncheck the "Published by default" checkbox.
5. Click on the "Save" button.


## Maintainers

- Andrey Vitushkin (wombatbuddy) - https://www.drupal.org/u/wombatbuddy
